---
layout: post
author: kele
date: "2017-12-7"
title: "我在做的事情"
tags: 
  - 学习使我快乐
  - 总结
  - 书籍
sticky: true
---
# 知识库

1. 记录看的书籍，文档，文章,电影，可以留作复习用       
1. 留着补坑
1. 填坑的时候最好听听歌--[我的歌单](http://music.163.com/playlist?id=399332367&userid=289286341)  
1. [电影语录、书摘](http://kele1997.xyz/2017/02/01/moviesandbooks/) 


2017-12-9  创建
## 爬虫 
* python网络数据采集 


## 人文
* 我们仨 杨绛
* 百年孤独 马尔克斯
* 活着 余华 
* 白鹿原 陈忠实
* 生死疲劳 莫言
* 白说 白岩松 #48%
* 看见  柴静 
* 西部招妻

## 心理学
* 暗时间 刘未鹏
* 微习惯 --蛮不错的，一天一个俯卧撑，小习惯容易坚持233

## 安全
* web安全深度剖析 #**看懂不到一半,留作复习用**  
* [php_bugs](https://github.com/bowu678/php_bugs)   
* [CTF_ALL_IN_ONE](https://github.com/firmianay/CTF-All-In-One)  **1.5.2**

## 网络
* [网络基本功](https://www.gitbook.com/book/wizardforcel/network-basic/details)    #看了一半
-[] 网络是怎样连接的 户根勤 #**看了不到一章**
* wireshark数据分析实战     

## 励志233
* 我是一只IT小小鸟  #跳读
* 我很重要 毕淑敏 (高三同桌送的233)  


## linux
* 笨方法学linux 
* linux network programming

## git 
* git学习指南 1-10章


## 电影
自己去看  --二战题材  
狂怒   ---二战  
辛德勒的名单   --二战  
最美心灵  
肖申克的救赎   
狩猎  
釜山行   
十二怒汉   
当幸福来敲门   
冒牌天神    
楚门的世界   
爱你罗茜      
比利·林恩的中场故事     
七宗罪     
招魂    
熔炉    
滚蛋吧！肿瘤君    
夜魔1    
夜魔2   
林中小屋   
小丑回魂  --有点后怕hiahia(￣▽￣)d
[飞越疯人院](https://www.bilibili.com/bangumi/play/ss12428)     
边城之城
人皮客栈   
变蝇人1
盗梦空间  
指环王    
博物馆奇妙夜1/2/3    
灵魂战车1/2    
地心历险记1/2   
末日崩塌    
撕裂的末日  
华尔街之狼      
消失的爱人   
无间道1    
无间道 2   
神探飞机头2 
蛊惑仔系列     
王牌特工    
王牌特工2     



## 科幻/动画 
银河护卫队1   
银河护卫队2   
爱宠大揭秘   
奇幻森林    
勇敢者游戏1   
勇敢者游戏2   
逃出克隆岛   
铁甲钢拳   
僵尸   
全面回忆     
明日边缘     




## 纪录片
浮生一日   
[巧克力-苦的真相  PANORAMA: CHOCOLATE - THE BITTER TRUTH （2010）](https://www.bilibili.com/watchlater/#/av11668885/p1)     
美国法医档案   # 蛮不错的       
德国的两部纪录片    


## 文档
* lxml的文档    


## TED 演讲   
[如何高效利用你的碎片时间](https://www.bilibili.com/video/av15641333/)   

[脆弱的强大力量](https://www.bilibili.com/video/av18198845/)  
[60秒告诉你人生到底是什么](https://www.bilibili.com/video/av18217846/)  


-----------------------------
## TODO:   


文章题目| 起始日期 |页数 | 进度|
---|---|----|----|
[windows kernel exploitation](https://rootkits.xyz/blog/) [中文版](https://zhuanlan.zhihu.com/p/32662568)|2018-1-16| 
[face_recognition](https://zhuanlan.zhihu.com/p/32662568)|2018-01-16 18:22:45|1|
[ctf狗怎么活到最后](https://zhuanlan.zhihu.com/p/32942490)|2018-01-16 18:32:26|1|
[phpadmin csrf漏洞删库](https://zhuanlan.zhihu.com/p/32658394)|2018-01-16 18:35:16 |1|
[常见web攻防总结](http://www.danding.net/2018/01/%E5%B8%B8%E8%A7%81-web-%E5%AE%89%E5%85%A8%E6%94%BB%E9%98%B2%E6%80%BB%E7%BB%93/)|2018-01-16 18:36:38|1|
[黑客修仙之道](https://bbs.ichunqiu.com/thread-32263-1-1.html?from=sec)|2018-01-16 18:37:32|1|
[网马解密](https://pan.baidu.com/share/link?uk=168697278&shareid=119342490)|2018-01-16 18:38:16|many
[开源cms漏洞](https://github.com/Mr5m1th/POC-Collect)|2018-01-16 18:38:45|1|
[tensortflow学习笔记](https://github.com/MachineLP/Tensorflow-)|2018-01-16 18:41:32|many
[php一句话](http://www.freebuf.com/articles/web/9396.html)|2018-02-16 00:14:08|many





TODO：
侧耳倾听
-----
Last_Update:   
2018-03-15 21:32:06
