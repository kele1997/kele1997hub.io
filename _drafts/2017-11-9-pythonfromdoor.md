---
layout: post
author: kele
date: 2017-11-9
title: "python 复习"
tags:
  - python
  - 总结
  - 学习使我快乐
---
# python 复习
就python的基本语法来说，python是非常容易入门的，但是如果只是简单的入门，对编程底层的东西不深入了解的话，底层不稳，很难建设上层建筑，所以决定复习一下pyhton,补一下坑      
ps.前几天一个群友，说python已经学完了，学完了list,dict，etc..，循环，数据库，基础，突然就想起当初的自己，学完python的语法之后，瞬间感觉有会了一门语言，但是当我接触到numpy matplotlib PIL lxml BeautifulSoup ....等等库的时候，发现自己差太多了，list推导式是我在一次看源码的过程中看到的，那是突然警觉，我的基础并不是太牢靠.......
[慕课网的文章](https://zhuanlan.zhihu.com/p/31212834)  


## 新手普遍存在的问题  
1. 不了解Python对象模型, 变量名, 对象概念含混      
[参考资料](https://www.cnblogs.com/aademeng/articles/7263047.html)
所有python中的值都是有对象或者对象之间的关系组成的    
python对象的三个重要特性 identity type value    
* identity  
identity可以理解为内存的地址，每一个对象都有唯一的一个地址，因此也只有唯一的一个id(identity)，两个对象可以使用`is`运算符进行比较是否是同一个对象 
* type    
type是对象的类型，决定了对象保存值得类型，使用内置函数type()查看对象的类型，返回一个的也是一个对象，而不是简单的字符串
* value  
对象表示的数据，值是可变的,叫做可变对象(mutable对象)，值一经创建就不可再变的对象成为(immutable对象),数字、字符串、元组是不可变对象，字典和列表是可变的对象  

* attitude
一个对象有各种属性,其中有三个内置的方法getattr setattr   hasattr[参考资料](https://www.cnblogs.com/cenyu/p/5713686.html)   


至于变量名使用表示一个变量的,就我理解来看，变量名应该是指向一个对象的内存值，而对象是一个类的实例化   

2. 不了解可变对象和不可变对象, 尤其是int.  
1. 

https://www.tuicool.com/articles/zUvyuiz


https://zhuanlan.zhihu.com/p/24828851     


2.可变对象和不可变对象  
可变对象:一旦创建之后还可以改变但是地址不会发生改变，也就是该变量指向的还是原来的对象。   
不可变对象:创建之后不能更改，如果更改，变量就会指向一个新的对象  
```
a=89#a是一个int类型的对象
id(a)#获取唯一的id值
a=26#对a重新赋值
id(a)#再次获取id值，可以发现id值已经变化了
```
或者是
```
def hapi()
  a=89
  a=26
import dis
dis.dis(hapi)
```
查看指令如下：  

      2           0 LOAD_CONST               1 (56)#载入const变量 56
                  3 STORE_FAST               0 (a)#快速存储将56存到a变量的内存位置

      3           6 LOAD_CONST               2 (44)#载入const变量44
                  9 STORE_FAST               0 (a)#快速存储44到a变量的内存
                12 LOAD_CONST               0 (None)
                15 RETURN_VALUE

int float tuple str都是不可变对象     
list dict是可变对象
   

3. 不了解切片意味着拷贝, 在循环中大量使用, 影响效率.
emmmmm，我一直因为这个东东是获取内存中的一段，涨知识了。。     
说说切片吧。
```
    >>> seq[:]                # [seq[0],   seq[1],          ..., seq[-1]    ]
    >>> seq[low:]             # [seq[low], seq[low+1],      ..., seq[-1]    ]
    >>> seq[:high]            # [seq[0],   seq[1],          ..., seq[high-1]]
    >>> seq[low:high]         # [seq[low], seq[low+1],      ..., seq[high-1]]
    >>> seq[::stride]         # [seq[0],   seq[stride],     ..., seq[-1]    ]
    >>> seq[low::stride]      # [seq[low], seq[low+stride], ..., seq[-1]    ]
    >>> seq[:high:stride]     # [seq[0],   seq[stride],     ..., seq[high-1]]
    >>> seq[low:high:stride]  # [seq[low], seq[low+stride], ..., seq[high-1]]  
```
这个是stackover上的答案，感觉满不错的，直接复制过来 
```
  >>> class slicee:
  ...     def __getitem__(self, item):
  ...         return `item`
  ...
  >>> slicee()[0, 1:2, ::5, ...]
  '(0, slice(1, 2, None), slice(None, None, 5), Ellipsis)'  
```
python当中反引号的作用:把其他格式转换成合理的字符格式，python3.x已经不支持了，需要使用repr()函数
    In [48]: print "hello world"+2333

    ----> 1 print "hello world"+2333

    TypeError: cannot concatenate 'str' and 'int' objects

    In [49]: print "hello world"+`2333`
    hello world2333

    In [50]: print "hello world"+repr(2333)
    hello world2333

4. 浅拷贝和深拷贝
[参考资料](http://python.jobbole.com/82294/)       
简单的来说就是，python里面的大多数复制都是使用了浅拷贝，类似于c语言中的引用，这样很节省空间，但是复制后的对象修改之后会影响到原来的对象，所以我们需要另一种复制方式，深拷贝copy.deepcopy()    
看上面那篇文章涉及到了**工厂函数**，工厂函数涉及到了涉及模式   
工厂函数:看起来像是函数，但是实际上是一个类，创建了一个对象    
常见的工厂函数:`list()`,`dir()`,`set()`,`bool()`,`type()`，etc
     


5. bytes和str  
python 2.7中的str 和 unicode   
[参考资料](http://python.jobbole.com/81244/)  


https://www.python-course.eu/python3_magic_methods.php 

http://www.cnblogs.com/piperck/p/6362203.html  

http://blog.csdn.net/u012829152/article/details/41911497




6. 经常在需要使用is的场景使用==
https://www.cnblogs.com/CheeseZH/p/5260560.html   
我觉得假如你认真的阅读了深拷贝和浅拷贝的内容，这段内容应该很好理解   
is 比较的是两个对象的id，也就是c语言中的地址     
== 比较的是两个对象的内容，value值 
```
str1="test"
str2=str1
str3="test"
print str1 is str2
print str1 == str2
print str1 is str3 
print str1 == str3
```
上面的例子里面str2=str1是浅拷贝，所以str2和str1是同一个id(类似于c中的地址)

7. def运行的执行语句   
[参考资料](http://blog.csdn.net/mathboylinlin/article/details/94014071)   
> def是Python中的一个可执行语句——函数并不存在，直到Python运行了def后才存在。def创建了一个对象，并将其赋值给一个变量名，这个变量名就是函数名。def可以出现在任一语句可以出现的地方——甚至是嵌套在其他语句中    
还是不难理解的，因为python语言是脚本语言，不想是c，java这样的强类型语言，需要进行编译之后才可以运行，def的函数，只有在运行的时候才真正的使用；python中的一切都是对象，所以函数也是一个对象,所以我们可以把def看作是创建一个函数对象的过程   
----
## List 列表推导式
```
list=[i for i in xrange(10)]    
list=[i for i in xrange(100) if i%2==1]
```

## List.sort()
s.sort([cmp[, key[, reverse]]]) 是这个函数的原型
reverse是True就进行反向排序，key可以付给一个自定义的比较函数    
list.sort()是对list类型进行永久排序，而且没有返回值   
list.sorted() 是对list进行临时排序，返回一个有序的list类型  

## python 当中的切片
list类型的索引可以使用切片
```
list=[i for i in range(20)]
print list[0:20:2]
```
这个切片的格式有三部分a:b:c a是链表的起点索引，b是终点索引，c是步长，
所以说有的时候我们可以是切片获得list的反转  list[::-1],起点默认是list的长度，终点是0，步长是-1,这样就是反向迭代了       
list.reverse()
numpy中还有一种矩阵的切片也是同样的道理

## 常用内置类型的
int float bool bytes str list dict set
emmmmmm,bytes dict set的使用 in is运算符    yield返回 id()


## 魔法函数
[资料](http://blog.csdn.net/yuan_j_y/article/details/9317817)


## 工厂函数


